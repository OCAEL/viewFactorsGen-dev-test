* Analytical result of view factors
** view factor between vetical rectangulars

Fij=0.20004376

https://www.sit.ac.jp/user/konishi/JPN/Freeware/GeometricFactorSIT/GeometricFactor-vertical.zip

** view factor between parallel rectangulars

Fij=0.199825

https://www.sit.ac.jp/user/konishi/JPN/Freeware/GeometricFactorSIT/GeometricFactor-parallel.zip
